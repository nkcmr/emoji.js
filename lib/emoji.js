var _ = require('lodash');
var punycode = require('punycode');
var debug = require('debug')('emoji');

var data = require('../emoji.json');

debug('building links...');
var start = new Date();

var use_variation = ['+1', '-1'];

// only use twitter icons
data = _.filter(data, 'twitter_img');
data = _.map(data, function(em) {
  if (_.contains(use_variation, em.short_name))
    em.short_name = em.short_names[1];

  return _.pick(em, 'short_name', 'unified', 'google', 'softbank', 'au', 'docomo', 'sheet_x', 'sheet_y');
});

var unified = {};
var softbank = {};
var google = {};
var kddi = {};
var docomo = {};

for (var i = 0; i < data.length; i++) {
  var id;

  // always set unified
  id = (data[i].unified.split('-')[0]).toLowerCase();
  unified[id] = data[i];

  if(data[i].softbank) {
    id = (data[i].softbank.split('-')[0]).toLowerCase();
    softbank[id] = data[i];
  }

  if (data[i].google) {
    id = data[i].google.toLowerCase();
    google[id] = data[i];
  }

  if (data[i].au) {
    id = (data[i].au.split('-')[0]).toLowerCase();
    kddi[id] = data[i];
  }

  if (data[i].docomo) {
    id = (data[i].docomo.split('-')[0]).toLowerCase();
    docomo[id] = data[i];
  }
}
debug('done building links (%dms)', (Date.now() - start.getTime()));

function splice(str, index, count, add) {
  debug('idx: %s, count: %s, add: %s', index, count, add);

  return str.slice(0, index) + add + str.slice(index + count);
}

function pad(num, size) {
  var s = num + '';
  while (s.length < size) s = '0' + s;
  return s;
}

function emoji(s) {
  var start = new Date();

  var codes = [];
  _.forEach(punycode.ucs2.decode(s), function(val, idx, arr) {
    if (!(val >= 0xfe00 && val <= 0xfe0f))
      codes.push(val);
  });

  s = punycode.ucs2.encode(_.clone(codes));

  var skip_next = false;
  var shift = 0;

  _.forEach(codes, function(val, idx, arr) {
    var base16, e, mate, next_bit, first, last, found, id_key;

    if (skip_next) {
      skip_next = false;
      return;
    }

    base16 = val.toString(16);

    if (base16.length < 4)
      base16 = pad(base16, 4);

    found = false;

    if (typeof unified[base16] === 'object') {
      found = true;
      e = unified[base16];
      id_key = 'unified';
    }

    if (!found && typeof google[base16] === 'object') {
      found = true;
      e = google[base16];
      id_key = 'google';
    }

    if (!found && typeof softbank[base16] === 'object') {
      found = true;
      e = softbank[base16];
      id_key = 'softbank';
    }

    if (!found && typeof kddi[base16] === 'object') {
      found = true;
      e = kddi[base16];
      id_key = 'au';
    }

    if (!found && typeof docomo[base16] === 'object') {
      found = true;
      e = docomo[base16];
      id_key = 'docomo';
    }

    if (found) {
      debug('found emoji! \'%s\' @ %d (%s)', e.short_name, idx + shift, id_key);

      if (e[id_key].split('-').length == 2) {
        mate = parseInt(e[id_key].split('-')[1], 16);
        next_bit = codes[idx + 1];

        if (mate === next_bit) {
          skip_next = true;
          s = splice(s, (idx + shift), 4, (':' + e.short_name + ':'));
          shift += e.short_name.length;
        }
      } else {
        s = splice(s, (idx + shift), 2, (':' + e.short_name + ':'));
        shift += e.short_name.length + 1;
      }

      debug('shift: %d', shift);
    }

  });

  debug('emoji: %dms', (Date.now()) - start.getTime());
  return s;
}

module.exports = emoji;
