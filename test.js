var emoji = require('./lib/emoji');
var punycode = require('punycode');

console.log(emoji('i love ☕️ and coding'));

console.log(emoji('i-love-🇩🇪-and-beer'));

console.log(emoji('🚦🚒'));

console.log(emoji('i am 💈 as well as 🚨 and a little bit of 🏡'));

console.log(emoji('😄😃😀😊☺️😉😍😘😚😗😙😜😝😛😳😁😔😌😒😞😣'));

console.log(emoji('🇩🇪🎉😍'));

console.log(emoji('🇩🇪🎉3️⃣🐠😍'));

// google
console.log(emoji(punycode.ucs2.encode([0xFE039, 0xFE013])));
